const express = require("express");
const path = require("path");
const request = require("request");
const port = process.argv.slice(2)[0];

const data = require("./data");
const heroesService = "http://localhost:8081";

const app = express();
app.use(express.json());

app.get("/threats", (req, res) => {
	res.send(data.threats);
});

app.post("/assignment", (req, res) => {
	request.post(
		{
			headers: { "content-type": "application/json" },
			url: `${heroesService}/hero/${req.body.heroId}`,
			body: `{
			"busy": true
		}`,
		},
		(err, heroResponse, body) => {
			if (!err) {
				const threatId = parseInt(req.body.threatId);
				const threat = data.threats.find((subject) => subject.id === threatId);
				threat.assignedHero = req.body.heroId;
				res.status(202).send(threat);
			} else {
				res.status(400).send({ problem: `Hero Service responded with issue ${err}` });
			}
		}
	);
});

app.use("/img", express.static(path.join(__dirname, "img")));

app.listen(port, () => {
	console.log("Running threat on port " + port);
});
