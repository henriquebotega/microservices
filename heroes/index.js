const express = require("express");
const path = require("path");
const port = process.argv.splice(2)[0];

const data = require("./data");
const app = express();

app.use(express.json());

app.get("/powers", (req, res) => {
	res.send(data.powers);
});

app.get("/heroes", (req, res) => {
	res.send(data.heroes);
});

app.post("/hero/**", (req, res) => {
	const heroId = parseInt(req.params[0]);
	const foundHero = data.heroes.find((subject) => subject.id === heroId);

	if (foundHero) {
		for (let attribute in foundHero) {
			if (req.body[attribute]) {
				foundHero[attribute] = req.body[attribute];
				console.log(`Set ${attribute} to ${req.body[attribute]} in hero: ${heroId}`);
			}
		}
		res.status(202)
			.header({ Location: `http://localhost:${port}/hero/${foundHero.id}` })
			.send(foundHero);
	} else {
		res.status(404).send();
	}
});

app.use("/img", express.static(path.join(__dirname, "img")));

app.listen(port, () => {
	console.log("Running on port " + port);
});
